import Image from "next/image";
import { Inter } from "next/font/google";
import IframePage from "../components/IframePage";

const inter = Inter({ subsets: ["latin"] });

export default function Home() {

  const filter3Value = "Madi%20Farahani,Jackie%20Salazar";
  const filter_1_value = "Paid"
  const filter_2_value = null

  const function0 = (arr: (string | undefined | null)[] = []) => {
    // split each value into new arrays and append to final array to display
    // 3 items max in final array
    let filtersToDisplay = [] as typeof arr
    for (const filters of arr) {
      if (filters) {
        const filterSet = filters.split(',')?.map((filter) => {
          console.log("function 0: " + filter + "include 20: " + filter.includes('%20'))
          return decodeURIComponent(filter);
        })
        filtersToDisplay = filtersToDisplay.concat(filterSet)
      }
    }

    return filtersToDisplay.join(', ')
  }


  const function1 = (arr: (string | undefined | null)[] = []) => {
    // split each value into new arrays and append to final array to display
    // 3 items max in final array
    let filtersToDisplay = [] as typeof arr
    for (const filters of arr) {
      if (filters) {
        const filterSet = filters.split(',')?.map((filter) => {
          console.log("function 1: " + filter + "include 20: " + filter.includes('%20'))

          if (filter.includes('%20')) {
            return filter.replace('%20', ' ')
          }
          return filter
        })
        filtersToDisplay = filtersToDisplay.concat(filterSet)
      }
    }

    return filtersToDisplay.join(', ')
  }


  const finalDisplay0 = function0([filter_1_value, filter_2_value, filter3Value])
  const finalDisplay1 = function1([filter_1_value, filter_2_value, filter3Value])

  return (
    <main
      className={`flex min-h-screen flex-col items-center justify-between p-4 ${inter.className}`}
    >

      <div>
        <div className="my-10">{finalDisplay0}</div>
        <div>{finalDisplay1}</div>
      </div>


      {/* <IframePage /> */}
    </main>
  );
}

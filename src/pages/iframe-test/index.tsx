
// pages/UrlFrame.tsx

import { useState } from 'react';
import Head from 'next/head';

const UrlFrame = () => {
  const [url, setUrl] = useState('');
  const [iframeSrc, setIframeSrc] = useState('');

  // Updates the URL state when user types in the input field
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setUrl(event.target.value);
  };

  // Sets the iframe source to render it when button is clicked
  const loadIframeContent = () => {
    if (url) {
      setIframeSrc(url);
    }
  };

  return (
    <div className="p-4 w-full">
      <Head>
        <title>URL Frame</title>
      </Head>

      {/* Input area for URL */}
      <input
        type="text"
        style={{
          border:"1px ",
          width: "100%"
        }}
        name="Please input url"
        placeholder="Please input url"
        value={url}
        onChange={handleInputChange}
        className="border p-2 w-full mb-4"
      />

      {/* Button to load iframe content */}
      <button onClick={loadIframeContent} className="bg-blue-500 text-white px-4 py-2 rounded">
        Load Iframe Content
      </button>

       {/* Iframe rendering area */}
       <div className={`mt-4 w-full bg-white ${!iframeSrc ? 'bg-white' : ''}`}>
           <iframe src={iframeSrc} width="100%" height="600px" frameBorder="0"></iframe>
       </div>
     </div>
   );
};

export default UrlFrame;